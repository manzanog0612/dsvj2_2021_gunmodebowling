﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]

public class GameManager : MonoBehaviour
{
    [Header("Game")]
    [SerializeField] bool win = false;
    [SerializeField] bool lose = false;

    const short amountPines = 10;
    public int pinesAlive = (int)(amountPines);
    public bool shootedOtherThing = false;
    
    //times
    private float timeToFinish = 1;
    private float timeUpdate = 2;

    void SetFinalData(PinesManager componentPines)
    {
        UpdatePinesCount(componentPines);

        int score = (componentPines.pines.Count - pinesAlive) * 10;

        SceneChanger.finalData = new SceneChanger.FinalUserData();

        if (SceneChanger.GetSceneName() == "Bowling")
        {
            SceneChanger.finalData.ballSpeed = GetComponentInChildren<BallManager>().speed;
            SceneChanger.finalData.tries = GetComponentInChildren<BallManager>().movementsLeft;
            SceneChanger.finalData.minSpeed = GetComponentInChildren<BallManager>().minSpeed;
            SceneChanger.finalData.maxSpeed = GetComponentInChildren<BallManager>().maxSpeed;
            SceneChanger.finalData.score = score;
        }
        else
        {
            SceneChanger.finalData.score = (amountPines - pinesAlive) * 10;
        }

        SceneChanger.finalData.pinesAlive = pinesAlive;
    }

    public bool CheckGameEnded()
    {
        return win || lose;
    }
    public bool CheckWin()
    {
        return pinesAlive == 0 && !lose;
    }

    public bool CheckLose()
    {
        PinesManager componentPines = GetComponentInChildren<PinesManager>();
        BallManager componentBallManager = GetComponentInChildren<BallManager>();

        return pinesAlive > 0 && componentBallManager.movementsLeft <= 0 && !win && !componentPines.CheckAnyMoving();
    }

    public bool CheckShootOtherThing()
    {
        return shootedOtherThing;
    }

    bool CheckRetry(BallManager componentBallManager, PinesManager componentPines)
    {
        return componentBallManager.trown && !componentBallManager.activated && !componentPines.CheckAnyMoving();
    }

    bool CheckTimeToUpdate()
    {
        return timeUpdate <= 0;
    }

    void UpdatePinesCount(PinesManager componentPines)
    {
        pinesAlive = componentPines.pines.Count;

        foreach (Pine.PineData pd in componentPines.pines)
        {
            if (pd.fallen && !pd.moving)
            {
                pinesAlive--;
            }
        }
    }

    void UpdateTimeForUpdates()
    {
        if (timeUpdate <= 0)
            timeUpdate = 1.5f;

        timeUpdate -= Time.deltaTime;        
    }

    void BowlingUpdate()
    {
        PinesManager componentPines = GetComponentInChildren<PinesManager>();
        BallManager componentBallManager = GetComponentInChildren<BallManager>();

        if (CheckTimeToUpdate()) UpdatePinesCount(componentPines);

        UpdateTimeForUpdates();

        if (CheckWin())
        {
            timeToFinish -= Time.deltaTime;

            if (timeToFinish <= 0)
            {
                win = true;
                SceneChanger.win = true;
                SetFinalData(componentPines);
            }

        }
        else if (CheckLose())
        {
            timeToFinish -= Time.deltaTime;

            if (timeToFinish <= 0)
            {
                lose = true;
                SceneChanger.lose = true;
                SetFinalData(componentPines);
            }
        }
        else if (CheckRetry(componentBallManager, componentPines))
            componentBallManager.ResetFirstValues();
    }

    void GunModeUpdate()
    {
        PinesManager componentPines = GetComponentInChildren<PinesManager>();

        UpdatePinesCount(componentPines);

        if (CheckWin())
        {
            win = true;
            SceneChanger.win = true;
            SetFinalData(componentPines);
        }
        else if (CheckShootOtherThing())
        {
            lose = true;
            SceneChanger.lose = true;
            SetFinalData(componentPines);
        }
    }

    void Update()
    {
        if (lose || win)
            return;

        if (SceneChanger.GetSceneName() == "Bowling")
        {
            BowlingUpdate();
        }
        else
        {
            GunModeUpdate();
        }



    }
}
