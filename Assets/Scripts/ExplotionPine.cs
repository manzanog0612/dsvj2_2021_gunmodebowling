﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionPine : MonoBehaviour
{
    Rigidbody body;
    public Vector3 initialForce = new Vector3();

    float timeFlying = 0.01f;

    void Start()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (timeFlying > 0)
        {
            body.velocity += initialForce;
            timeFlying -= Time.deltaTime;
        }
    }
}
