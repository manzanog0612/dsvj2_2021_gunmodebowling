﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Pine : MonoBehaviour
{
    //Bowling
    public static short totalPines = 0;

    static float radius;
    static float height;

    public class PineData
    {        
        public short id = 0;

        public bool moving = false;
        public bool fallen = false;
        
        public Vector3 pos = new Vector3();
    }

    PineData data;

    [SerializeField] public bool moving;
    [SerializeField] public bool fallen;

    //Gun mode  

    /*public class ExplotionPines
    //{
        //GameObject prefab = GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().ePinePrefab;

        //public Vector3 pos = new Vector3();
        //public Vector3 initialForce = new Vector3();

        //public Rigidbody body;

        //int amountPines = 0;
        //int amountCircunferences = 1;

        /*void SetAmountCircunferencesOfInstancing()
        {
            const float maxAngle = 360.0f;
            const float minAngle = 15.0f;
            float angle = 0;

            for (int i = 0; i < amountPines; i++)
            {
                angle += minAngle;

                if (angle == maxAngle)
                {
                    amountCircunferences++;
                    angle = 0;
                }
            }
        }*/

    /*float GetDistanceAxisBetweenPines(float radius)
    {
        return amountPines / amountCircunferences / 360.0f;
    }*/

    /*public void Instantiation()
    {
        float axis = 0.0f;
        float nextSpawnAngle = GetDistanceAxisBetweenPines(radius);
        float initialRadius = radius;

        for (int i = 0; i < amountCircunferences; i++)
        {
            while (axis < 360.0f)
            {
                if (nextSpawnAngle == axis)
                {
                    GameObject eP = Instantiate(prefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
                    ePines.Add
                }
                axis++;
            }

            radius += initialRadius;
            axis = 0.0f;
        }

    }*/
    //}


    public PineData GetData()
    {
        return data;
    }

    public void InitData()
    {
        data = new PineData();
        totalPines++;
        data.id = totalPines;
    }

    static bool IsMoving(Rigidbody body)
    {
        const float num = 1.5f;
        
        bool one = body.velocity.x > num || body.velocity.x < -num;
        bool two = body.velocity.y > num || body.velocity.y < -num;
        bool three = body.velocity.z > num || body.velocity.z < -num;

        return (one || two || three);
    }

    private bool CheckFell()
    {
        bool one = transform.rotation.x > 0.2 || transform.rotation.x < -0.2;
        bool two = transform.rotation.z > 0.2 || transform.rotation.z < -0.2;

        return (one || two);
    }

    bool CheckPineHit(GameObject go)
    {
        return go.tag == "Pine";
    }

    void Start()
    {
        name = "Pine " + data.id;

        if (SceneChanger.GetSceneName() == "Bowling") return;

        radius = gameObject.transform.localScale.x / 2.0f;
        height = gameObject.transform.localScale.y;
        gameObject.AddComponent<ExplotionPinesManager>();
        
    }

    void Update()
    {
        GameObject gm = GameObject.FindWithTag("GameManager");
        Rigidbody body = GetComponent<Rigidbody>();

        if (gm.GetComponent<GameManager>().CheckGameEnded())
            return;
        
        if (!data.moving && IsMoving(body))
            data.moving = true;

        if (data.moving && !IsMoving(body))
        { 
            body.velocity = new Vector3(0, 0, 0);
            data.moving = false;
        }

        if (CheckFell() && !data.fallen)
            data.fallen = true;

        moving = data.moving;
        fallen = data.fallen;
        data.pos = transform.position;
    }

    public void OnMouseDown()
    {
        if (SceneChanger.GetSceneName() == "Bowling") return;

        Vector3 outOfScreenPosition = new Vector3(gameObject.transform.position.x, -20, gameObject.transform.position.z);
        float diferenceInOriginalPinePosY = -(-20 - gameObject.transform.position.y);

        if (Input.GetMouseButtonDown(0))
        {
            GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().pines.Remove(data);
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<ExplotionPinesManager>().Instantiation(gameObject, diferenceInOriginalPinePosY);
            gameObject.transform.position = outOfScreenPosition;
        }
    }
}
